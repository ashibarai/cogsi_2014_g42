/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.jnjmx.todd;

/**
 *
 * @author cister
 */
public class COGSITrabalho implements COGSITrabalhoMBean
{
    private int cogsiValue;
    
    public COGSITrabalho()
    {
        cogsiValue = 0;
    }

    @Override
    public void grow() {
        cogsiValue += 1;
    }

    @Override
    public void grow(int increment) {
        cogsiValue += increment;
    }

    @Override
    public void reset() {
        cogsiValue = 0;
    }
    
    @Override
    public void decrease() {
        if ((cogsiValue - 3) > 0)
        {
			cogsiValue -= 3;
		}
		else
		{
			// prevents cogsiValue to be negative
			cogsiValue = 0;
		}
    }

    @Override
    public Integer getValue() {
        return cogsiValue;
    }
}
