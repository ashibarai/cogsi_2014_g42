package net.jnjmx.todd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;

public class JMXToddServerSessionsStatus {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// Connect to a remote MBean Server
			JMXConnector c = javax.management.remote.JMXConnectorFactory
					.connect(new JMXServiceURL(
							"service:jmx:rmi:///jndi/rmi://127.0.0.1:10500/jmxrmi"));

			MBeanServerConnection mbs = c.getMBeanServerConnection();

			// Lets try to access the MBean net.jnjmx.todd.Server:
			ObjectName son = new ObjectName("todd:id=SessionPool");
			ObjectInstance ob=mbs.getObjectInstance(son);

			Integer availableSessions=(Integer)mbs.getAttribute(son, "AvailableSessions");

			Integer size=(Integer)mbs.getAttribute(son, "Size");

			if ((availableSessions.doubleValue()/size.doubleValue())>=0.25) {
				System.out.println("Sessions OK");						
				c.close();
				System.exit(0);
			} else {
				System.out.println("Need more Sessions!!!");						
				c.close();
				System.exit(2);
			}

			
			//System.out.println("Todd server is UP");						
						

			c.close();
			
			System.exit(0);
		} catch (Exception ex) {
			System.out.println("Error: unable to connect to MBean Server");
			System.exit(2);
		}

	}

}
