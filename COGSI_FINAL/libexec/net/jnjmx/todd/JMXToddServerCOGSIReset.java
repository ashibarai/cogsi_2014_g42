package net.jnjmx.todd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Set;

import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;

public class JMXToddServerCOGSIReset {

	public static final int RESET = 1;
	public static final int DECREASE = 2;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			int action = 0;
			if (args.length > 0) {
				try {
					action = Integer.parseInt(args[0]);
				} catch (NumberFormatException e) {
					System.err.println("Argument" + args[0] + " must be an integer.");
					System.exit(1);
				}
			}
			
			// Connect to a remote MBean Server
			JMXConnector c = javax.management.remote.JMXConnectorFactory
					.connect(new JMXServiceURL(
							"service:jmx:rmi:///jndi/rmi://127.0.0.1:10500/jmxrmi"));

			MBeanServerConnection mbs = c.getMBeanServerConnection();

			// Lets try to access the MBean net.jnjmx.todd.Server:
			ObjectName son = new ObjectName("todd:id=CogsiTrabalho");
			ObjectInstance ob=mbs.getObjectInstance(son);

			Integer cogsiValue = (Integer)mbs.getAttribute(son, "Value");

			if (action == RESET) // reset is needed
			{
				mbs.invoke(son, "reset", null, null);
			}
			else if (action == DECREASE) // try to decrease value
			{
				mbs.invoke(son, "decrease", null, null);
			}
			
			c.close();
			System.exit(0); /* ok */
		} catch (Exception ex) {
			System.out.println("Error: unable to connect to MBean Server");
			System.exit(2);
		}

	}

}
