package net.jnjmx.todd;

import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;

public class JMXToddServerCOGSIStatus {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// Connect to a remote MBean Server
			JMXConnector c = javax.management.remote.JMXConnectorFactory
					.connect(new JMXServiceURL(
							"service:jmx:rmi:///jndi/rmi://127.0.0.1:10500/jmxrmi"));

			MBeanServerConnection mbs = c.getMBeanServerConnection();

			// Lets try to access the MBean net.jnjmx.todd.Server:
			ObjectName son = new ObjectName("todd:id=CogsiTrabalho");
			ObjectInstance ob=mbs.getObjectInstance(son);

			Integer cogsiValue=(Integer)mbs.getAttribute(son, "Value");

			if (cogsiValue <= 5) {
				System.out.println("COGSI Value OK");						
				System.exit(0);
			}
			else if (cogsiValue <= 10)
			{
				System.out.println("COGSI Value is too high: " + cogsiValue + " ! Trying to decrease value!");
				System.exit(1);
			}
			else {
				System.out.println("COGSI Value needs reset!!! Will reset!");
				System.exit(2);
			}

			c.close();
			
			System.exit(0);
		} catch (Exception ex) {
			System.out.println("Error: unable to connect to MBean Server");
			System.exit(2);
		}

	}

}
